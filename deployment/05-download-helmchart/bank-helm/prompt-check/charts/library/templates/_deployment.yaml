{{- define "library.deployment" -}}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Chart.Name }}
  labels:
    {{- include "library.labels" . | nindent 4 }}
spec:
  {{- if not ((.Values.autoscaling).enabled) }}
  replicas: {{ .Values.replicaCount | default 1 }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "library.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      annotations:
        {{- range $filename := .Values.propertyFiles }}
        {{- $configFile := (tpl ( $filename.name ) $) }}
        {{- $annotation := $configFile | replace "/" "-" | lower }}
        {{- $filetype := $filename.type | default "data" }}
        {{- if eq $filetype "binaryData"}}
        {{ $annotation }} : {{ $.Files.Get $configFile | sha256sum }}
        {{- else }}
        {{ $annotation }} : {{ tpl (($.Files.Glob $configFile ).AsConfig | indent 2 ) $ | sha256sum }}
        {{- end }}
        {{- end }}
        {{- if ((.Values.dbSecret).enabled) }}
        checksum/secret:  {{ include ("library.secret") . | sha256sum }}
        {{- end }}
      {{- with .Values.podAnnotations }}
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        {{- include "library.selectorLabels" . | nindent 8 }}
    spec:
      {{- if include "library.imagePullSecrets" . }}
      imagePullSecrets:
        {{- include "library.imagePullSecrets" . | nindent 8 }}
      {{- end }}
      {{- with .Values.hostAliases }}
      hostAliases:
        {{- tpl (toYaml .) $ | nindent 8 }}
      {{- end }}
      {{- if ( include  "library.serviceAccountName" . ) }}
      serviceAccountName: {{ include "library.serviceAccountName" . }}
      {{- end }}
      {{- if .Values.securityContext }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      {{- end }}
      {{- if .Values.extraInitContainers }}
      initContainers:
        {{- with .Values.extraInitContainers }}
        {{- tpl . $ | nindent 8 }}
        {{- end }}
      {{- end }}
      containers:
        - name: {{ .Chart.Name }}
          {{- if .Values.securityContext }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          {{- end }}
          image: "{{ include "library.imageRepository" . }}:{{ include "library.imageTag" . }}"
          imagePullPolicy: {{ include "library.imagePullPolicy" . }}
          {{- with .Values.lifecycleHooks }}
          lifecycle:
            {{- tpl . $ | nindent 12 }}
          {{- end }}
          env:
            {{- $javaToolOptionsFound := false }}
            {{- $newenv := merge .Values.injectPodEnv .Values.extraEnv }}
            {{- range $key, $value := $newenv  | default nil }}
            - name: {{ $key }}
            {{- if and (eq $key "JAVA_TOOL_OPTIONS") $.Values.otel_enabled  }}
            {{- $javaToolOptionsFound = true }}
              value: {{ tpl $value $ }} -javaagent:/opentelemetry/opentelemetry-javaagent.jar
            {{- else }}
              value: {{ tpl (quote $value) $ }}
            {{- end }}
            {{- end }}
            {{- if .Values.otel_enabled }}
            - name: logging.pattern.level
              value:  {{ .Values.global.otel_pattern | default "[%mdc{trace_id}:%mdc{span_id}:%mdc{trace_flags}] %5p" | quote }}
            - name: otel.exporter.otlp.endpoint
              value: {{ .Values.global.otel_url | default "http://signoz-otel-collector.monitoring:4317" }} 
            - name: otel.logs.exporter
              value: {{ .Values.global.otel_logs_exporter | default "otlp" }} 
            - name: otel.metrics.exporter
              value: {{ .Values.global.otel_metrics_exporter | default "otlp" }} 
            - name: otel.resource.attributes
              value: service.name={{ .Release.Namespace }}-{{ .Chart.Name }}
            {{- if not $javaToolOptionsFound }}
            - name: JAVA_TOOL_OPTIONS
              value: "-javaagent:/opentelemetry/opentelemetry-javaagent.jar"
            {{- end }}
            {{- end }}
          envFrom:
            {{- with .Values.injectEnvFrom }}
              {{- tpl . $ | nindent 12 }}
            {{- end }}
            {{- with .Values.extraEnvFrom }}
              {{- tpl . $ | nindent 12 }}
            {{- end }}
          ports:
            - name: http
              containerPort: {{ .Values.appPort | default 8080 }}
              protocol: TCP
            {{- with .Values.extraPorts }}
            {{- toYaml . | nindent 12 }}
            {{- end }}
          {{- with .Values.livenessProbe }}
          livenessProbe:
            {{- tpl . $ | nindent 12 }}
          {{- end }}
          {{- with .Values.readinessProbe }}
          readinessProbe:
            {{- tpl . $ | nindent 12 }}
          {{- end }}
          {{- with .Values.startupProbe }}
          startupProbe:
            {{- tpl . $ | nindent 12 }}
          {{- end }}
          {{- if .Values.resources }}
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
          {{- end }}
          volumeMounts:
            {{- with .Values.injectVolumeMounts }}
              {{- tpl . $ | nindent 12 }}
            {{- end }}
            {{- with .Values.extraVolumeMounts }}
              {{- tpl . $ | nindent 12 }}
            {{- end }}
        {{- with .Values.extraContainers }}
        {{- tpl . $ | nindent 8 }}
        {{- end }}
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.affinity }}
      affinity:
        {{- tpl ( toYaml . ) $ | nindent 8 }}
      {{- end }}
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      volumes:
        {{- with .Values.injectVolumes }}
          {{- tpl . $ | nindent 8 }}
        {{- end }}
        {{- with .Values.extraVolumes }}
          {{- tpl . $ | nindent 8 }}
        {{- end }}
{{- end -}}