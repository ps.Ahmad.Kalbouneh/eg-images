#!/bin/bash
## update IP and Nodes IP before execute this script.
##( Change the belew IP to your IP address )
IP="10.39.1.85"
## make sure from IP inside ngnix.conf (include all k8s nodes)
## Update the below nodeIP to your (vagrant IP or host node IP)
nodeIP=("192.168.56.13")
## configing /ect/hosts 
HostName=("ecc-core.progressoft.dev" "ecc-frontend.progressoft.dev" "ecc-ftp.progressoft.dev" "vault.progressoft.dev" "ps-security.progressoft.dev")
for hn in ${HostName[@]}
 do
         bash ./hostadd.sh addhost $IP $hn
 done
## on your Local PC, install nginx by running
sudo apt -y install nginx
## start the services 
sudo systemctl start nginx 
## add below configuration at the end on nginx.conf file sudo vi /etc/nginx/nginx.conf
## make sure that the IP inside nginx.conf is correct (vagrant IP or host node IP)
sudo sed -i."bkb" 's/192.168.56.11/'${nodeIP[@]}'/g' ./nginx.conf
sudo  cp nginx.conf /etc/nginx/nginx.conf  
## restart 
sudo systemctl restart nginx 
sudo systemctl status nginx 
