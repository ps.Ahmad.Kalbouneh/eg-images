#!/bin/bash
# make sure you are login before with script login.sh and add all images inside .images-list.txt files
# you can use the script ( ./login.sh ).

list="images-list.txt"
images="images.tar"

for i in $(cat ${list}); do
	docker pull ${i}
done

docker save $(cat ${list} | tr '\n' ' ') > ${images}

mv *.tar ../