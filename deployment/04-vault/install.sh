##################################################################################################################
##git clone https://github.com/hashicorp/vault-helm.git
 
## Install vault app using helm chart:
##################################################################################################################
  
helm upgrade --install vault vault-helm --timeout 1200s --create-namespace --namespace ecc -f vault-helm/Chart.yaml
 
##################################################################################################################
 ## Create a Virtual Service for vault portal:
##################################################################################################################

kubectl apply -f vault_vs_template.yaml -n ecc

##################################################################################################################
 ## Initialize the Vault server by running the below command:
 ## Take a copy of the output ( seal keys and root).
 ## important to keep the root key and unseal keys secure and protected, as they grant access to sensitive data stored in the Vault.
##################################################################################################################

kubectl exec -it vault-0 -n ecc -- vault operator init                       

##################################################################################################################
## Run the below command 3 times as below, and providing the unseal keys generated in the above step :
## Each time you have to provide one of the different key from those 5 unseal keys
##################################################################################################################

kubectl exec -it vault-0 -n ecc -- vault operator unseal  ##>> qbFyf7QN+miAQAP8bOXhBiCqOJ5TCRzGFmBB6K+P/5ho
kubectl exec -it vault-0 -n ecc -- vault operator unseal  ##>> R8fmhjyH7xKY3DXswMmk/yZO7rkp7BusISTYCSRrFD/f
kubectl exec -it vault-0 -n ecc -- vault operator unseal  ##>> 3qaBJJUAA7BIENZChdxgFzlH9GbIdSM36YEG7UUQ7AGw

 ##################################################################################################################

 ## make sure url is working 
 https://vault.progressoft.dev/
 ##################################################################################################################
 ## on local env you will not able to view ssl or proceed , follow below to fix the issue : 
 ## if you use Self SSL then (firefox)
 ## advance >>  View Certificate >> Download PEM (cert)PEM (chain)  ( Download both) .
 ## then back to setting >> search on certificates >>  view  certificates  >> authorities >> import >> progressoft-dev-chain.pem
 ## done !
##################################################################################################################
# update below on firefox : 
 1- open firefox
 2- put on the url >>>   about:config
 3- serach on param : stricttransportsecurity >>> update it to "fales" 
# REF : 
# https://www.iodocs.com/how-do-i-turn-off-http-strict-transport-security/

## create engine
 1- open vault URL :https://vault.progressoft.dev/
 2- create kv prompt-check >> type local
 3- Enable new engine
 4- Type: Generic >> KV
 5- Enable a Secrets Engine:
    Path >> prompt-check
    Version >> 1
    Chick option local

############################################################
# login to vault and enable transit
kubectl exec -it vault-0 -n ecc -- sh

vault login $(token)
 
vault secrets enable -path=prompt-check-transit transit
vault write -f prompt-check-transit/keys/images
############################################################